Fonte: [https://bluevisionbraskem.com/inovacao/conheca-seis-aplicativos-que-ajudam-a-promover-a-reciclagem/#:~:text=Na apresentação%2C o Cataki destaca,aplicativo pode ser baixado gratuitamente](https://bluevisionbraskem.com/inovacao/conheca-seis-aplicativos-que-ajudam-a-promover-a-reciclagem/#:~:text=Na%20apresenta%C3%A7%C3%A3o%2C%20o%20Cataki%20destaca,aplicativo%20pode%20ser%20baixado%20gratuitamente).

# **Cataki**

[cataki.org/pt](https://www.cataki.org/pt/) | [play.google.com/store/apps/details?id=com.ionicframework.pimp473818](https://play.google.com/store/apps/details?id=com.ionicframework.pimp473818)

Nesta plataforma, é possível cadastrar um indivíduo ou uma cooperativa. Na apresentação, o Cataki destaca que existem hoje, no Brasil, 800.000 trabalhadores na coleta seletiva de lixo, mas apenas 300 deles estão cadastrados. Para ampliar a rede, basta registrar o catador ou catadora que atua na sua rua, no seu bairro ou na sua região. O aplicativo pode ser baixado gratuitamente.

Se quiser descartar algum material ou resíduo reciclável, basta usar o app para encontrar os trabalhadores mais próximos ao local da coleta. O trabalho da organização é aberto, sem fins lucrativos e colaborativo. Se você quiser ajudar como voluntário, pode entrar em contato com os realizadores da iniciativa.

# **Desafio Ambiental**

[https://play.google.com/store/apps/details?id=com.aminprojects.saveearth](https://play.google.com/store/apps/details?id=com.aminprojects.saveearth)

Desafio Ambiental é o seu guia para fazer mudanças no planeta e torná-lo melhor lugar para você e as gerações futuras, este aplicativo irá oferecer-lhe:

- Desafios a serem dados, pontos para pontuar e níveis para alcançar.
- Notícias diárias sobre o meio ambiente,
- Qualidade do Ar em Tempo Real em sua cidade e país.
- Detector de poluição sonora.
- Eventos sobre o meio ambiente
- Poluição e qualidade da água em seu país.
- Ecossistema e estado de vegetação em seu país.

# Intellibins

Esta iniciativa foi desenvolvida especialmente para ajudar os moradores de Nova York a encontrarem incentivos para reciclar, reutilizar e doar. O aplicativo mapeia a cidade e mostra onde é possível encaminhar não apenas plástico, vidro e papel, mas também roupas e eletrônicos. Compatível com Android e iOS, a tecnologia permite procurar o local mais próximo de você. No total, são mais de 1.500 pontos de reciclagem e 21 tipos de materiais diferentes.

# **Litterati**

[litterati.org/](https://www.litterati.org/) | [play.google.com/store/apps/details?id=org.litterati.android&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1](https://play.google.com/store/apps/details?id=org.litterati.android&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)

Internacional, este aplicativo propõe que as pessoas tirem uma foto do lixo que encontrarem (garrafas, bitucas de cigarro, papéis e outros). Então, elas devem colocar palavras-chaves, marcar a localização geográfica (geotag) e o horário onde o lixo foi encontrado. O mapa é capaz de mostrar que tipo de lixo se acumula e em qual lugar, norteando ações específicas para evitar o problema. Um exemplo de solução aconteceu em uma escola na Califórnia, quando o mapa indicou que o lixo encontrado com mais frequência no pátio eram os saquinhos de canudos descartáveis da cantina. A escola, então, decidiu parar de oferecer esse item para os estudantes.

# **Moeda Verde**

[https://play.google.com/store/apps/details?id=br.com.moedaverde.app](https://play.google.com/store/apps/details?id=br.com.moedaverde.app)

O Moeda Verde é o app que te recompensa por adotar hábitos que transformam a comunidade em um ambiente mais sustentável. Acumule Moeda Verde e troque por recompensas reais oferecidas por empresas que acreditam em um mundo melhor!

Conheça os hábitos que você pode adotar e acumular Moedas Verdes:

- Preferir a bike;
- Caminhar;
- Descarte correto de materiais em pontos de coleta;
- Consumo consciente;
- Assistir vídeos educativos;
- Doar alimentos, roupas entre outras necessidades da comunidade;
- Participar de eventos educativos;

# **Manual de Etiqueta – Planeta Sustentável**

Disponível apenas para a plataforma IOS, o aplicativo é grátis e traz informações sobre como mudar alguns hábitos de forma simples e reduzir os impactos ambientais que cometemos no dia a dia. Dentro do Manual estão dicas de comportamento voltado à sustentabilidade social e utilização responsável de energia. Bilíngue (português e inglês), a plataforma reúne 50 práticas divididas por temas: uso da água, energia elétrica, cidadania, reciclagem e consumo, classificadas pelo nível de esforço e pela dimensão do impacto no cotidiano.

# **Sustentabilizando**

[https://play.google.com/store/apps/details?id=com.doublesoftwares.sustentabilizando](https://play.google.com/store/apps/details?id=com.doublesoftwares.sustentabilizando)

O Sustentabilizando é um projeto educativo e tem o intuito alertar os usuários quanto ao desperdício dos recursos naturais e também o que pode ser feito para reduzir os gastos dos mesmos.

- Veja os tipos de materiais e saiba quais você pode ou não reciclar.
- Calcule o seu consumo de energia e evite o desperdício.
- Selecione uma atividade e calcule o seu consumo de água.
- Decore as cores das lixeiras de uma forma interativa.
- Teste seus conhecimentos jogando o Jogo da Reciclagem.
- Divirta-se com o Jogo da Memória.
- Controle o tempo de banho em sua casa, veja os gastos gerados e colabore com o seu bolso e com o planeta.
- Controle o consumo de energia e contribua para o desenvolvimento sustentável do nosso planeta.
- Utilize nossos conversores e simule os consumos de água e energia bem como os custos gerados.